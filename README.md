node-webkit Wercker Box
=======================

[![wercker status](https://app.wercker.com/status/0d60996c0357639f4f5f1813e98b964c/m "wercker status")](https://app.wercker.com/project/bykey/0d60996c0357639f4f5f1813e98b964c)

Wercker box with the essentials to test and build a node-webkit app.

Link to the [node-webkit box on wercker](https://app.wercker.com/#search/boxes/node-webkit).

# License

The MIT License (MIT)
